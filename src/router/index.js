import Vue from 'vue'
import Router from 'vue-router'
import slot from '@/pages/slot'
import flow from '@/pages/flow'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'slot',
      component: slot
    },
    {
      path: '/flow',
      name: 'flow',
      component: flow
    }
  ]
})
